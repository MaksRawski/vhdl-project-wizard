library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FILE_TB is
end entity;

architecture sim of FILE_TB is
	constant frequency : integer := 100e6; -- 100 MHZ
	constant period    : time    := 1000 ms / frequency;
	constant datawidth : integer := 8;		

	-- specify component
	component FILE_component
		generic(datawidth : integer := datawidth);
		port(
			clk,rst : in  std_logic;
			output  : out unsigned(datawidth downto 0)
		);
	end component;

	-- connect FILE_component with entity in work.FILE
	for FILE: FILE_component use entity work.FILE;

	-- specify the signals
	signal clk : std_logic := '1';
	signal rst : std_logic := '0';
	signal output : unsigned(datawidth downto 0);
begin
	-- create FILE_component and map signals
	FILE: FILE_component
	port map(
		clk => clk,
		rst => rst,
		output => output
	);

	-- clock process
	clk <= not clk after period/2; 

	-- testbench process
	process
	begin
		wait for period; -- pass default signals (the ones above)

	end process;
end architecture;

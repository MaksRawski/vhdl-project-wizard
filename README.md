# VHDL project wizard
Create new projects from a template and easily compile them with a Makefile.

## Requirements

* `ghdl` either mcode or gcc backend 
* `make`

## Usage
1. `./init.sh` to run the wizard.
2. `make` to compile your project.
3. `make run` to run your testbench.
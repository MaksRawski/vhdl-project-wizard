#!/bin/bash

in="$(ls | grep -o -i -P '^((?!tb).)*\.vhd.?')"
in_tb="$(ls | grep -o -i -P '.*tb.*\.vhd.?')"

function decide {
	case $decision in
	"N" | "n")
		echo "N"
		;;
	*)
		echo "T"
		;;
	esac
}

function replace {
	out="$(echo $in | cut -d . -f 1)"
	out_tb="$(echo $in_tb | cut -d . -f 1)"

	sed -i "s/FILE_TB/$out_tb/g" .gitignore
	sed -i "s/FILE_TB/$out_tb/g" Makefile
	sed -i "s/FILE_TB/$out_tb/g" hdl-prj.json
	sed -i "s/FILE_TB/$out_tb/g" $in
	sed -i "s/FILE_TB/$out_tb/g" $in_tb

	sed -i "s/FILE/$out/g" .gitignore
	sed -i "s/FILE/$out/g" Makefile
	sed -i "s/FILE/$out/g" hdl-prj.json
	sed -i "s/FILE/$out/g" $in
	sed -i "s/FILE/$out/g" $in_tb
	
	echo ""
	read -p "Should the default stop time be 1us? [Y/n]" decision
	if [ "$(decide)" == "N" ]; then
		echo "Then what should it be? (don't put space between integer and the unit)"
		time=""
		while [ "$time" == "" ]; do
			read -p "> " time
		done;
		sed -i "s/1us/$time/g" Makefile
	fi;

	echo ""
	read -p "Do you want to initalize a new git repo? [Y/n]" decision
	if [ "$(decide)" == "T" ]; then
		[ "$(git config --get remote.origin.url)" == "https://gitlab.com/i4mz3r0/vhdl-project-wizard" ] && (rm -rf .git; git init)
		rm LICENSE README.md
	fi;
	
	echo ""
	echo "Everything should be set up. Have fun!"
	exit 0;
}

if [ "$in" != "FILE.vhd" ]; then
	echo "Seems like you have manually changed the file name.";
	read -p "Do you want to continue? [Y/n]" decision

	if [ "$(decide)" == "T" ]; then
		if [ "$in_tb" == "FILE_TB.vhd" ]; then
			echo "You forgot to name testbench file. What should it be (without extension)?"
			in_tb=""
			while [ "$in_tb" == "" ]; do
				read -p "> " in_tb
			done;

			in_tb="${in_tb}.vhd"
			mv FILE_TB.vhd "$in_tb"
		fi;
		replace
	else
		echo "Exiting"
		exit 0
	fi;
fi;

echo "Welcome to VHDL project wizard!"
echo "How would you like to call your base file (without extension)?"

in=""
in_tb=""

while [ "$in" == "" ]; do
	read -p "> " in
done;

echo ""
read -p "Should testbench be called ${in}_tb.vhd? [Y/n]" decision

if [ "$(decide)" == "T" ]; then
	in_tb="${in}_tb.vhd"	
else
	echo "So what should it be (without extension)?"
	while [ "$in_tb" == "" ]; do
		read -p "> " in_tb
	done;
fi;

in="${in}.vhd"
mv FILE.vhd "$in"
mv FILE_TB.vhd "$in_tb"
replace

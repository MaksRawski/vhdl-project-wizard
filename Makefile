in = FILE.vhd
in_tb = FILE_TB.vhd

out    = $(basename $(in) .vhd)
out_tb = $(basename $(in_tb) .vhd)

GFLAGS = -g --std=08 -frelaxed-rules -fsynopsys --ieee=standard

all: $(out)

$(out): $(in) $(in_tb)
	ghdl -a $(GFLAGS) $(in)
	ghdl -a $(GFLAGS) $(in_tb)
	ghdl -e $(GFLAGS) $(out)
	ghdl -e $(GFLAGS) $(out_tb)
	
clean:
	rm -f *.cf
	rm -f *.o
	rm -f $(out)
	rm -f $(out_tb)
	rm -f waveform.vcd

run: $(out_tb)
	@echo "--------- START OF OUTPUT --------"
	@ghdl -r --std=08 $(out_tb) --stop-time=1us --vcd="waveform.vcd"
	@echo "--------- END OF OUTPUT --------"
